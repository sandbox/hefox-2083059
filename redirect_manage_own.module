<?php
/**
 * @file
 * Code for the Redirect manage own.
 */


/**
 * Implements hook_permission().
 */
function redirect_manage_own_permission() {
  return array(
    'manage own redirects' => array(
      'title' => t('Create own redirects'),
      'description' => t("Let's a use create and edit redirects"),
    ),
  );
}

/**
 * Implements hook_redirect_access().
 */
function redirect_manage_own_redirect_access($op, $redirect, $account) {
  if ($op == 'create') {
    return user_access('manage own redirects', $account) ? REDIRECT_ACCESS_ALLOW : REDIRECT_ACCESS_IGNORE;
  }
  elseif ($redirect->uid  == $account->uid) {
    return REDIRECT_ACCESS_ALLOW;
  }
}

/**
 * Implements hook_redirect_access().
 */
function redirect_manage_own_redirect_presave($redirect) {
  global $user;
  if (empty($redirect->uid) && empty($redirect->rid)) {
    // @see https://drupal.org/node/2051313
    // Temporary fix so not to rely on a patch.
    $redirect->uid = $user->uid;
  }
  // I don't feel like implementing a form alter just to redirect here after
  // creating a new redirect.
  $item = menu_get_item();
  if ($item['path'] == 'user/%/redirect/add') {
    $_GET['destination'] = 'user/' . $user->uid . '/redirect';
  }
}

/**
 * Implements hook_menu().
 */
function redirect_manage_own_menu() {
  $items['user/%user/redirect'] = array(
    'title' => 'Redirects',
    'description' => 'Redirect users from one URL to another.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('redirect_list_form'),
    'access callback' => 'redirect_manage_own_current_user_and_permission',
    'access arguments' => array(1, 'view'),
    'file' => 'redirect.admin.inc',
    'file path' => drupal_get_path('module', 'redirect'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 100,
    'file path' => drupal_get_path('module', 'redirect'),
  );
  $items['user/%user/redirect/list'] = array(
    'title' => 'List',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );
  $items['user/%user/redirect/add'] = array(
    'title' => 'Add redirect',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('redirect_edit_form'),
    'access callback' => 'redirect_manage_own_current_user_and_permission',
    'access arguments' => array(1, 'create', 'redirect'),
    'file' => 'redirect.admin.inc',
    'type' => MENU_LOCAL_ACTION,
    'file path' => drupal_get_path('module', 'redirect'),
  );
  $items['user/%user/redirect/edit/%redirect'] = array(
    'title' => 'Edit redirect',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('redirect_edit_form', 5),
    'access callback' => 'redirect_access',
    'access arguments' => array(1, 'update', 4),
    'file' => 'redirect.admin.inc',
    'file path' => drupal_get_path('module', 'redirect'),
  );
  $items['user/%user/redirect/delete/%redirect'] = array(
    'title' => 'Delete redirect',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('redirect_delete_form', 5),
    'access callback' => 'redirect_access',
    'access arguments' => array(1, 'delete', 4),
    'file' => 'redirect.admin.inc',
    'file path' => drupal_get_path('module', 'redirect'),
  );
  return $items;
}

function redirect_manage_own_current_user_and_permission($account, $op, $redirect = NULL) {
  global $user;
  if ($account->uid !== $user->uid) {
    return FALSE;
  }
  if ($op == 'view') {
    return user_access('manage own redirects', $account);
  }
  elseif ($redirect) {
    return redirect_access($op, $redirect, $account);
  }
}

/**
 * Implements hook_query_TAG_alter().
 */
function redirect_manage_own_query_redirect_access_alter(QueryAlterableInterface $query) {
  global $user;
  if (arg(0) == 'user' && arg(2) == 'redirect') {
    $query->condition('r.uid', $user->uid);
  }
}
